#include <vector>
#include <string>
#include "pal.h"
#include <fstream>
#include <iostream>
Palindrome::Palindrome() //constructor
{

}

int Palindrome::readFile(std::string fileName) //reads file contents, adds them to wordList
{
    try {
    std::ifstream myFile(fileName);
    if (myFile.is_open())
    {
        std::string newText;
        while (std::getline(myFile, newText))
        {
            this->wordList.push_back(newText);
        }
    myFile.close();
    return 1;
    }
    else
    {
        throw 10; 
    }
    }
    catch (int e)
    {
        std::cout << "An exception occurred. No file with given name of " << fileName << " present.\n";
        return 0;
    }
}

void Palindrome::updatePalTotal() //checks size of palindrome vector, updates palTotal
{
    this->palTotal = this->palindromes.size();
}
void Palindrome::findPals() //finds pals in wordlist, inserts the in palindromes vector                    
{
    std::string word;
    std::string reversedWord;
    std::string letter;
    for (int i = 0; i < this->wordList.size(); i++)
    {
        word = this->wordList.at(i);
        letter.clear();
        reversedWord.clear();
        for(int j = 0; j < word.size(); j++)
        {
            letter = word.at((word.length() - 1) - j);
            reversedWord.insert(j, letter);
        }
        if (word == reversedWord) //check if reversed word and word are same.
        {
            this->palindromes.push_back(word);
        }
        
    }
    this->updatePalTotal(); //count num of pals and set member palcount
}
void Palindrome::printPals() //prints out the list of palindromes found
{
    std::cout << "\nThe following are palindromes from the file:" <<  std::endl;
    for (int i = 0; i < this->palTotal; i++)
    {
        std::cout << this->palindromes.at(i) << std::endl;
    }
    std::cout << "\n" << this->palTotal << " palindrome(s) present in the file.\n";
}
