
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include "pal.h"

int main (int argc, char * argv[] )
{
    std::string fileName;
    Palindrome myPal;
    int fileReadOK = 0;
    while (fileReadOK == 0)
    {
        if (argc != 2)
        {
            std::cout << "\nEnter the name of the text file: " << std::endl;
            std::cin >> fileName;
        }
        else
        {
            fileName = argv[1];
        }
        
        if (myPal.readFile(fileName))
        {
            fileReadOK = 1;
        }
    }
    
    myPal.findPals();
    myPal.printPals();
}