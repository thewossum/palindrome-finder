#ifndef PAL_H
#define PAL_H
#include <vector>
#include <string>

class Palindrome
{
    private:
        std::vector <std::string> palindromes; //total palindromes from file
        std::vector <std::string> wordList; //all words from file
        int palTotal; //sum of palindromes found 
    public:
        int readFile(std::string fileName); //reads file contents, adds them to wordList
        void updatePalTotal(); //checks size of palindrome vector, updates palTotal
        void findPals(); //finds pals in wordlist, inserts the in palindromes vector                    
        void printPals(); //prints out the list of palindromes found
        Palindrome();
};
#endif